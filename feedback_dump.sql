-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: feedback
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `name` varchar(50) DEFAULT 'anonymous',
  `rating` varchar(20) DEFAULT NULL,
  `speaker` varchar(250) DEFAULT NULL,
  `volunteers` varchar(250) DEFAULT NULL,
  `club` varchar(250) DEFAULT NULL,
  `session` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES ('Anonymous','Good','Nothing','Nothing','Nothing','Informative'),('Thrivikram Mudunuri','Okay','Better organisation of content delivery and a more engaging talk. ','','They did their best with limited resources.','You guys tried.'),('Anonymous','Okay','Communication.','','',''),('Anonymous','Okay','Clarity in conveying the info','','',''),('Anonymous','Good','Better communication....','Nothing','Nothing','Worked with electronic components for the first time'),('Anonymous','Okay','He should be little bit more interactive and should be able to control the crowd!','Should be more interactive!','Nothing ! Good lunch maybe! ','The components'),('Melrick Mercenary','Okay','Volume and clarity. ','Idk. ','Get us better coffee pls. ','Components. '),('A man has no name','Bad','More engagement with participant ','A little bit more engagement would be better.','Non organised. time management can be improved. Management need to be worked on, Recommendation follow chain of command','A better start with the hardware '),('Anonymous','Good','Nil','Nil','Nil','Hands on sessions'),('Anonymous','Excellent','Be louder and motivating','-','-','Interactive'),('Anonymous','Good','More of hands on teaching rather than chalkboard teaching','None','Time management ','The practical section'),('Prajwala ','Good','','','',''),('Prajwala ','Good','','','',''),('Anonymous','Excellent','','','',''),('Anonymous','Okay','Better explanation and focus more on programming portion','Better class management ','Better time management ','Explanation of basic concept '),('Anonymous','Good','i want him to be more loud and clear.','they should explain each and every concept personally to all groups.','Dont waste time in starting the event.','The way it was conducted and the immense knowledge that i gained from it.'),('Anonymous','Okay','','','',''),('Anonymous','Excellent','','','',''),('Anonymous','Excellent','','','','11; delete from feedback;'),('Anonymous','Excellent','k','k','k','byugjbj'),('Anonymous','Excellent','','','\"); <?php mysql_query($conn,\"show tables\"); ?> die(); ',''),('Anonymous','Excellent','','','\"); <?php $q=mysql_query($conn,\"show tables\"); echo $q ?>  ','');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-06  8:46:52
