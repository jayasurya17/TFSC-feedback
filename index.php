<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>TFSC Feedback - Arduino Workshop Day 1</title>
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <style type="text/css">        
        .wrapper {
            max-width: 80%;
            overflow: hidden;
            background-color: white;
            padding: 20px;
        }
        .wrapper h1{
            font-family: "Courier New", Courier, monospace;
        }
        body{
            padding: 0 10% 20px 10% !important;
            background-color: #ffd42a;
        }
        textarea{
            resize: none;
            width: 100%;
        }
        .submit{
            margin-top: 20px;
        }
        @media screen and (max-width : 640px)
        {
            .wrapper h2{
                font-size: 20px;
            }
            body{
                padding:10px;
                padding-top: 10vh;
            }
	.wrapper{
		margin: 0px !important;
		min-width: 100%;
        }
	.wrapper h3{
		font-size: 15px;
	}
	}
    </style>

  </head>

  <body>
    <div class="container wrapper">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="jumbotron text-center">
                    <img src="index.svg" height="100%" width="40%"><h2>Arduino workshop feedback</h2>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <form action="feedback.php" method="post">
                    <hr>
                    <h3>Name</h3>
                    <input type="text" placeholder="Optional" name="name">


                    <h3>How would you rate this session?</h3>
                    <input type="radio" name="platform" value="Excellent" checked> Excellent<br>
                    <input type="radio" name="platform" value="Good"> Good<br>
                    <input type="radio" name="platform" value="Okay"> Okay<br>
                    <input type="radio" name="platform" value="Bad"> Bad<br><hr>

                    
		    <h3>What did you like about the session?</h3>
                    <textarea rows="4" name="session"></textarea><br>

                    <h3>What improvements would you like to see in the speaker?</h3>
                    <textarea rows="4" name="speaker"></textarea><br>


                    <h3>What improvements would you like to see in the external volunteers?</h3>
                    <textarea rows="4" name="volunteers"></textarea><br>


                    <h3>What improvements would you like to see in the organisers?</h3>
                    <textarea rows="4" name="club"></textarea><br>

                    
                    <div class="submit text-center">
                    <input type="submit" value="Submit" name="submit" class="btn btn-success">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
