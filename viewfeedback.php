<html>
	<head>
	<title>Workshop Feedback</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/style.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en" />
    <link href="/style.css" type="text/css" rel="stylesheet">
</head>
<body class="container-fluid">
<?php
	$con=mysqli_connect("localhost","root","1by14cs053","feedback");
	// Check connection
	if (mysqli_connect_errno())
	{
		echo "Failed to connect to Server: " . mysqli_connect_error();
	}
	$sql = "SELECT * from feedback";
	$result = mysqli_query($con,$sql);
echo   "<table class='table table-hover table-stripped'>
	<tr>
	<th>Name</th>
	<th>How would you rate this session?</th>			
	<th>What did you like about the session?</th>			
	<th>What improvements would you like to see in the speaker?</th>			
	<th>What improvements would you like to see in the external volunteers?</th>			
	<th>What improvements would you like to see in the organisers?</th>			
	</tr>";
	while($row = mysqli_fetch_assoc($result)){
		echo "<tr>
		<td>".$row['name']."</td>		
		<td>".$row['rating']."</td>		
		<td>".$row['session']."</td>		
		<td>".$row['speaker']."</td>		
		<td>".$row['volunteers']."</td>		
		<td>".$row['club']."</td>		
		</tr>";
	}
	echo "</table>";
	mysqli_close($con);
?>
</body>
</html>
